var password = document.getElementsByName("password")[0]
  , confirm_password = document.getElementsByName("confirm-password")[0];

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Пароли не совпадают.");
  } else {
    confirm_password.setCustomValidity('');
  }
}
