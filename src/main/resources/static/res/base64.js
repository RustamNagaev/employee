function encodeImgtoBase64(element) {
  var file = element.files[0];
  if (file.size > 1024 * 400) {
     var myModal = new bootstrap.Modal(document.getElementById('fileSizeOver')); 
     myModal.show();
  } else {
    var reader = new FileReader();
    reader.onloadend = function() {
      $("#convertImg").attr("href",reader.result);
      $("#convertImg").text(reader.result);
      $("#base64Img").attr("src", reader.result);
    }
    reader.readAsDataURL(file);
  }
}

function copyImageScrIntoInput(base64Val) {
    var p = document.getElementById("base64Img")
    base64Val.value = p.src;
}
