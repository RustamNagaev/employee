/*
drop table app_user;
drop table app_role;
drop table employee;
drop table juridical_face;
drop table department;
drop table "position";
*/

create table juridical_face (
    id    bigserial primary key 
  , name  varchar(100) not null	
);

create table department (
    id    bigserial primary key
  , name  varchar(100) not null	
);

create table position (
    id    bigserial primary key
  , name  varchar(100) not null	
);

create table employee (
    id                 bigserial primary key
  , juridical_face_id  bigint not null
  , department_id      bigint
  , position_id        bigint not null
  , name               varchar(100) not null
  , tech_stack         varchar(200)
  , experience         varchar(2000)
  , begin_work         date not null
  , education          varchar(200)
  , dismiss            date
  , dismiss_reason     varchar(200)
  , image              text
  , constraint fk_juridical_face foreign key (juridical_face_id) references juridical_face (id)
  , constraint fk_department foreign key (department_id) references department (id)
  , constraint fk_position foreign key (position_id) references "position" (id)
);

create table app_role (
    id           bigserial primary key
  , name         varchar(100) not null	
  , description  varchar(100) 
);


create table app_user (
    id            bigserial primary key
  , first_name    varchar(40) not null
  , last_name     varchar(40) not null
  , middle_name   varchar(40) 
  , email         varchar(100) not null
  , hash_password varchar(200) not null
  , role_id       bigint not null
  , constraint fk_role foreign key (role_id) references app_role (id)
);


