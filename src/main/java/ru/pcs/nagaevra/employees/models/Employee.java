package ru.pcs.nagaevra.employees.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "juridical_face_id")
    private JuridicalFace juridicalFace;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "position_id")
    private Position position;

    private String name;
    private String techStack;
    private String experience;
    private LocalDate beginWork;
    private String education;
    private LocalDate dismiss;
    private String dismissReason;

    @Column(name = "image", columnDefinition="text")
    private String image;
}
