package ru.pcs.nagaevra.employees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.nagaevra.employees.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
