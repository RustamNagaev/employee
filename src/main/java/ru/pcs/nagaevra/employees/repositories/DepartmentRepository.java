package ru.pcs.nagaevra.employees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.nagaevra.employees.models.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

}
