package ru.pcs.nagaevra.employees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.nagaevra.employees.models.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

}