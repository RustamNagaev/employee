package ru.pcs.nagaevra.employees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.nagaevra.employees.models.Position;

public interface PositionRepository extends JpaRepository<Position, Long> {

}
