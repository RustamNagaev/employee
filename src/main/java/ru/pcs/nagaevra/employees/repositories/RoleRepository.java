package ru.pcs.nagaevra.employees.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.nagaevra.employees.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
