package ru.pcs.nagaevra.employees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.nagaevra.employees.models.JuridicalFace;

public interface JuridicalFaceRepository extends JpaRepository<JuridicalFace, Long> {

}
