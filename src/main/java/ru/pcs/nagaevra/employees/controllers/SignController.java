package ru.pcs.nagaevra.employees.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.nagaevra.employees.dtos.UserDto;
import ru.pcs.nagaevra.employees.services.RoleService;
import ru.pcs.nagaevra.employees.services.SignUpService;

import java.util.Map;

@RequiredArgsConstructor
@Controller
public class SignController {

    private final SignUpService signUpService;
    private final RoleService roleService;

    @GetMapping("/signUp")
    public String getSignUpPage(Model model) {
        model.addAttribute("roles", roleService.findAll());
        return "signUp";
    }

    @PostMapping("/signUp")
    public String singUpUser(UserDto userDto) {
        signUpService.signUpUser(userDto);
        return "redirect:/signIn";
    }

    @GetMapping("/signIn" )
    public String getSignInPage(Model model, @RequestParam Map<String,String> allParams) {
        model.addAttribute("errorSignIn", allParams.containsKey("error") ? "errorSignIn" : "noErrorSignIn");
        return "signIn";
    }
}