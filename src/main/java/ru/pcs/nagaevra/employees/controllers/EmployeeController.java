package ru.pcs.nagaevra.employees.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.nagaevra.employees.dtos.EmployeeDto;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.models.Employee;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.services.*;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class EmployeeController {

    private final EmployeeService employeesService;
    private final PositionService positionsService;
    private final JuridicalFaceService juridicalFacesService;
    private final DepartmentService departmentsService;

    @GetMapping("/listEmployee")
    public String getEmployeesPage(Model model) {
        listOfEmployee(model, "");
        return "listEmployee";
    }

    @GetMapping("/searchEmployee")
    public String getEmployeesPage(Model model, @RequestParam("searchName") String searchName) {
        listOfEmployee(model, searchName);
        return "listEmployee";
    }

    private void listOfEmployee(Model model, String searchName) {
        List<Employee> employees;
        if (searchName.isEmpty())  {
            employees = employeesService.findAll();
        } else {
            employees = employeesService.findAllByName(searchName);
        }
        model.addAttribute("searchName", searchName);
        model.addAttribute("employees", employees);
    }

    @GetMapping("/addEmployee")
    public String getAddEmployeePage(Model model) {

        List<Position> positions = positionsService.findAll();
        List<JuridicalFace> juridicalFaces = juridicalFacesService.findAll();
        List<Department> departments = departmentsService.findAll();

        model.addAttribute("positions", positions);
        model.addAttribute("juridicalFaces", juridicalFaces);
        model.addAttribute("departments", departments);

        return "addEmployee";
    }

    @PostMapping("/addEmployee")
    public String addEmployee(EmployeeDto employeeDto, BindingResult bindingResult) {

        System.out.println(bindingResult.getAllErrors().toString());

        employeesService.save(employeeDto);

        return "redirect:/listEmployee";
    }


    @GetMapping("/listEmployee/{employeeId}/detailEmployee")
    public String getEmployeePage(Model model, @PathVariable("employeeId") Long employeeId) {

        EmployeeDto employee = employeesService.getEmployeeAsDto(employeeId);

        List<Position> positions = positionsService.findAll();
        List<JuridicalFace> juridicalFaces = juridicalFacesService.findAll();
        List<Department> departments = departmentsService.findAll();


        model.addAttribute("employee", employee);
        model.addAttribute("positions", positions);
        model.addAttribute("juridicalFaces", juridicalFaces);
        model.addAttribute("departments", departments);

        return "detailEmployee";
    }

    @PostMapping("/listEmployee/{employeeId}/updateEmployee")
    public String updateEmployee(EmployeeDto employeeDto, @PathVariable("employeeId") Long employeeId) {

        employeeDto.setId(employeeId);

        employeesService.save(employeeDto);

        return "redirect:/listEmployee";
    }

    @PostMapping("/listEmployee/{employeeId}/deleteEmployee")
    public String deleteEmployee(@PathVariable("employeeId") Long employeeId) {

        employeesService.delete(employeeId);

        return "redirect:/listEmployee";
    }

}
