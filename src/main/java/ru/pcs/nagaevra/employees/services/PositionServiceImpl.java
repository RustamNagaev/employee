package ru.pcs.nagaevra.employees.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.repositories.PositionRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PositionServiceImpl implements PositionService{

    private final PositionRepository positionsRepository;

    @Override
    public List<Position> findAll() {
        return positionsRepository.findAll();
    }

}
