package ru.pcs.nagaevra.employees.services;

import ru.pcs.nagaevra.employees.dtos.UserDto;

public interface SignUpService {

    void signUpUser(UserDto userDto);

}
