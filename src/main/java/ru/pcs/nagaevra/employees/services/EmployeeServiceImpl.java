package ru.pcs.nagaevra.employees.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.dtos.EmployeeDto;
import ru.pcs.nagaevra.employees.exceptions.EmployeeNotFoundException;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.models.Employee;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.repositories.EmployeeRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.*;

@RequiredArgsConstructor
@Service
public class EmployeeServiceImpl implements  EmployeeService {

    private final EmployeeRepository employeesRepository;

    @Override
    public EmployeeDto getEmployeeAsDto(Long id) {

        Employee employee = employeesRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);

        if (employee.getImage() == null) employee.setImage("");

        EmployeeDto.EmployeeDtoBuilder employeeDtoBuilder = EmployeeDto.builder()
                .id(employee.getId())
                .juridicalFaceId(employee.getJuridicalFace() != null ? employee.getJuridicalFace().getId() : -1)
                .departmentId(employee.getDepartment() != null ? employee.getDepartment().getId() : -1)
                .positionId(employee.getPosition() != null ? employee.getPosition().getId() : -1)
                .beginWork(employee.getBeginWork() != null ? employee.getBeginWork().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) : "")
                .dismiss(employee.getDismiss() != null ? employee.getDismiss().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) : "")
                .name(employee.getName())
                .techStack(employee.getTechStack())
                .education(employee.getEducation())
                .experience(employee.getExperience())
                .dismissReason(employee.getDismissReason())
                .image(employee.getImage());


        return employeeDtoBuilder.build();
    }

    @Override
    public List<Employee> findAll() {
        List<Employee> employees = employeesRepository.findAll();
        employees.stream().forEach(employee -> {if (employee.getImage() == null) employee.setImage("");});
        return employees;
    }

    @Override
    public List<Employee> findAllByName(String searchName) {
        Example<Employee> example = Example.of(
                Employee.builder()
                        .name(searchName)
                        .position(Position.builder().name(searchName).build())
                        .build()
                , ExampleMatcher.matchingAny()
                        .withIgnoreCase()
                        .withMatcher("name", contains())
                        .withMatcher("position.name", contains())
        );

        return employeesRepository.findAll(example);
    }

    @Override
    public void save(EmployeeDto employeeDto) {

        Employee.EmployeeBuilder employeeBuilder = Employee.builder()
                .id(employeeDto.getId())
                .juridicalFace(employeeDto.getJuridicalFaceId() != null ? JuridicalFace.builder().id(employeeDto.getJuridicalFaceId()).build() : null)
                .department(employeeDto.getDepartmentId() != null ? Department.builder().id(employeeDto.getDepartmentId()).build() : null)
                .position(employeeDto.getPositionId() != null ? Position.builder().id(employeeDto.getPositionId()).build() : null);

        if (employeeDto.getBeginWork() != null)
            if (!employeeDto.getBeginWork().isEmpty())
                employeeBuilder.beginWork(LocalDate.parse(employeeDto.getBeginWork()));

        if (employeeDto.getDismiss() != null)
            if (!employeeDto.getDismiss().isEmpty())
                employeeBuilder.dismiss(LocalDate.parse(employeeDto.getDismiss()));

        employeeBuilder.name(employeeDto.getName())
                .techStack(employeeDto.getTechStack())
                .experience(employeeDto.getExperience())
                .education(employeeDto.getEducation())
                .dismissReason(employeeDto.getDismissReason())
                .image(employeeDto.getImage());

        Employee employee = employeeBuilder.build();

        employeesRepository.save(employee);

    }

    @Override
    public void delete(Long id) {
        employeesRepository.delete(employeesRepository.getById(id));
    }
}
