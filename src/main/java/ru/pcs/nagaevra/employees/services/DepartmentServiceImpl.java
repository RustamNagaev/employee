package ru.pcs.nagaevra.employees.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.repositories.DepartmentRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentsRepository;

    @Override
    public List<Department> findAll() {
        return departmentsRepository.findAll();
    }

}
