package ru.pcs.nagaevra.employees.services;

import ru.pcs.nagaevra.employees.models.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> findAll();

}
