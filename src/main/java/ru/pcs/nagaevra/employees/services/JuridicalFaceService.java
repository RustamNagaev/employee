package ru.pcs.nagaevra.employees.services;

import ru.pcs.nagaevra.employees.models.JuridicalFace;

import java.util.List;

public interface JuridicalFaceService {

    List<JuridicalFace> findAll();

}
