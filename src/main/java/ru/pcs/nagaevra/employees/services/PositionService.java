package ru.pcs.nagaevra.employees.services;

import ru.pcs.nagaevra.employees.models.Position;

import java.util.List;

public interface PositionService {

    List<Position> findAll();

}
