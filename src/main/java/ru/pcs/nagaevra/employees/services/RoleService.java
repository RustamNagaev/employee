package ru.pcs.nagaevra.employees.services;

import ru.pcs.nagaevra.employees.models.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();

}
