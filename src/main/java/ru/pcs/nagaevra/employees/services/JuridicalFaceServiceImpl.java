package ru.pcs.nagaevra.employees.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.repositories.JuridicalFaceRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class JuridicalFaceServiceImpl implements JuridicalFaceService{

    private final JuridicalFaceRepository juridicalFacesRepository;

    @Override
    public List<JuridicalFace> findAll() {
        return juridicalFacesRepository.findAll();
    }

}
