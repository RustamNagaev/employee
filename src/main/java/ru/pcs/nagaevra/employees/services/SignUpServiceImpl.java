package ru.pcs.nagaevra.employees.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.dtos.UserDto;
import ru.pcs.nagaevra.employees.models.Role;
import ru.pcs.nagaevra.employees.models.User;
import ru.pcs.nagaevra.employees.repositories.UserRepository;

@RequiredArgsConstructor
@Service
public class SignUpServiceImpl implements SignUpService{

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void signUpUser(UserDto userDto) {
        User user = User.builder()
                .email(userDto.getEmail())
                .hashPassword(passwordEncoder.encode(userDto.getPassword()))
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .middleName(userDto.getMiddleName())
                .role(Role.builder().id(userDto.getRoleId()).build())
                .build();

        userRepository.save(user);
    }
}
