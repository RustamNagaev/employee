package ru.pcs.nagaevra.employees.services;

import ru.pcs.nagaevra.employees.dtos.EmployeeDto;
import ru.pcs.nagaevra.employees.models.Employee;
import java.util.List;

public interface EmployeeService {

    EmployeeDto getEmployeeAsDto(Long id);

    List<Employee> findAll();
    List<Employee> findAllByName(String searchName);

    void save(EmployeeDto employeeDto);
    void delete(Long id);

}
