package ru.pcs.nagaevra.employees;


import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.repositories.DepartmentRepository;
import ru.pcs.nagaevra.employees.repositories.JuridicalFaceRepository;
import ru.pcs.nagaevra.employees.repositories.PositionRepository;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@SpringBootTest
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@AutoConfigureEmbeddedDatabase(type = AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES,
        beanName = "dataSource", provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY,
        refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_EACH_TEST_METHOD)
@DisplayName("Repositories tests:")
public class RepositoriesTest {

    @Autowired
    private DepartmentRepository departmentsRepository;

    @Autowired
    private PositionRepository positionsRepository;

    @Autowired
    private JuridicalFaceRepository juridicalFacesRepository;

    @Test
    public void department_repository_test() {
        String testDepartmentName = "Test department";
        String notFoundMessage = testDepartmentName + " not found";
        boolean findResult = false;
        Department department;

        // example for find department
        Example<Department> example = Example.of(
                Department.builder()
                        .name(testDepartmentName).build()
                , ExampleMatcher.matching()
                        .withMatcher("name", contains())
        );

        try {
            // save department
            departmentsRepository.save(new Department( null, testDepartmentName, null));
            // find department
            department = departmentsRepository.findOne(example).orElseThrow((Supplier<Throwable>) () -> new RuntimeException(notFoundMessage));
            // delete department
            departmentsRepository.delete(department);
            try {
                // find after delete
                departmentsRepository.findOne(example).orElseThrow((Supplier<Throwable>) () -> new RuntimeException(notFoundMessage));
            } catch (Throwable e) {
                findResult = true;
            }
        } catch (Throwable e) {
            fail(e.getMessage());
        }

        assertTrue(findResult);
    }

    @Test
    public void position_repository_test() {
        String testPositionName = "Test position";
        String notFoundMessage = testPositionName + " not found";
        boolean findResult = false;
        Position position;

        // example for find position
        Example<Position> example = Example.of(
                Position.builder()
                        .name(testPositionName).build()
                , ExampleMatcher.matching()
                        .withMatcher("name", contains())
        );

        try {
            // save position
            positionsRepository.save(new Position( null, testPositionName, null));
            // find position
            position = positionsRepository.findOne(example).orElseThrow((Supplier<Throwable>) () -> new RuntimeException(notFoundMessage));
            // delete position
            positionsRepository.delete(position);
            try {
                // find after delete
                positionsRepository.findOne(example).orElseThrow((Supplier<Throwable>) () -> new RuntimeException(notFoundMessage));
            } catch (Throwable e) {
                findResult = true;
            }
        } catch (Throwable e) {
            fail(e.getMessage());
        }

        assertTrue(findResult);
    }

    @Test
    public void juridical_face_repository_test() {
        String testJuridicalFaceName = "Test position";
        String notFoundMessage = testJuridicalFaceName + " not found";
        boolean findResult = false;
        JuridicalFace juridicalFace;

        // example for find juridical face
        Example<JuridicalFace> example = Example.of(
                JuridicalFace.builder()
                        .name(testJuridicalFaceName).build()
                , ExampleMatcher.matching()
                        .withMatcher("name", contains())
        );

        try {
            // save juridical face
            juridicalFacesRepository.save(new JuridicalFace( null, testJuridicalFaceName, null));
            // find juridical face
            juridicalFace = juridicalFacesRepository.findOne(example).orElseThrow((Supplier<Throwable>) () -> new RuntimeException(notFoundMessage));
            // delete juridical face
            juridicalFacesRepository.delete(juridicalFace);
            try {
                // find after delete
                juridicalFacesRepository.findOne(example).orElseThrow((Supplier<Throwable>) () -> new RuntimeException(notFoundMessage));
            } catch (Throwable e) {
                findResult = true;
            }
        } catch (Throwable e) {
            fail(e.getMessage());
        }

        assertTrue(findResult);
    }

}
