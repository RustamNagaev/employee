package ru.pcs.nagaevra.employees;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.pcs.nagaevra.employees.dtos.EmployeeDto;
import ru.pcs.nagaevra.employees.models.Employee;
import ru.pcs.nagaevra.employees.services.EmployeeService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@AutoConfigureEmbeddedDatabase(type = AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES,
        beanName = "dataSource", provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY,
        refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_EACH_TEST_METHOD)
@DisplayName("Employee service tests:")
@Sql(scripts = {"classpath:employee_dml.sql"})
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void get_employee_as_dto() {
        EmployeeDto employeeDto = employeeService.getEmployeeAsDto(1L);
        assertNotNull(employeeDto);
    }

    @Test
    public void find_all_employee() {
        List<Employee> employees = employeeService.findAll();
        assertEquals(4, employees.size());
    }

    @Test
    public void find_all_employee_by_name() {
        EmployeeDto employeeDto = employeeService.getEmployeeAsDto(1L);
        List<Employee> employees = employeeService.findAllByName(employeeDto.getName());
        assertEquals(1, employees.size());
    }

    @Test
    public void save_employee() {
        EmployeeDto employeeDto = EmployeeDto.builder()
                .name("Test employee")
                .juridicalFaceId(1L)
                .departmentId(1L)
                .positionId(1L)
                .beginWork("2021-12-31")
                .techStack("Test tech. stack")
                .experience("Test experience")
                .education("Test education")
                .dismiss("2021-12-31")
                .dismissReason("Test dismiss reason")
                .build();
        employeeService.save(employeeDto);
        List<Employee> employees = employeeService.findAllByName(employeeDto.getName());
        assertEquals(1, employees.size());
    }

    @Test
    public void delete_employee() {
        long employee_count = employeeService.findAll().size();
        employeeService.delete(2L);
        assertEquals((employee_count-1), employeeService.findAll().size());
    }
}
